import React, {useState,useEffect} from 'react';
import axios from "axios";
import Showintable from './Showintable'
import './Table.css'


const Table = () => {
    const [userData, setuserData] = useState([]);
    const [userError, setuserError] = useState("");


    useEffect(() => {
        axios({
            method: "get",
            url: `https://5f5ca5325e3a4d0016249893.mockapi.io/users`,
            responseType: "json",
        })
        .then((response) => {
            console.log(response, "receive");
            setuserData(response.data);
        })
        .catch((error) => {
            setuserError(error.message);
        });
    }, []);

    return(
        <div style={{padding:10,margin:20}}>
            <Showintable userlist = {userData} />
        </div>
    )
}

export default Table;