import React from 'react';
import Tutorial from './Tutorial';


// class Props extends React.Component{

//     constructor(Props){
//         super(Props);

//     }

//     render(){
//         return <div>
//             <h1>PROPS TUTORIAL</h1>
//             <h1>This is {this.props.language}</h1>  
//         </div>
//     }
// }

function Props(props){
    return(
        <div>
            <h1>This is example of Props</h1>
            <h1>This is {props.language}</h1>
        </div>
    )
}



export default Props;