import React, {useState, useEffect} from "react";
import axios from "axios";
import Table from './Table'
import './Table.css'

const Showintable = (props) => {
    const [Name, setName] = useState("");
    const [City, setCity] = useState("");
    const [Age, setAge] = useState("");
    const [State, setState] = useState("");
    const [Country, setCountry] = useState("");
    const [Editdata, setEditdata] = useState({});
    const [userData, setuserData] = useState([]);
    const [userError, setuserError] = useState("");


    const getmethod = () => {
      axios({
        method: "get",
        url: `https://5f5ca5325e3a4d0016249893.mockapi.io/users`,
        responseType: "json",
    })
    .then((response) => {
        console.log(response, "receive");
        setuserData(response.data);
    })
    .catch((error) => {
        setuserError(error.message);
    });
    }

    useEffect(() => {

      getmethod()
      // axios({
      //     method: "get",
      //     url: `https://5f5ca5325e3a4d0016249893.mockapi.io/users`,
      //     responseType: "json",
      // })
      // .then((response) => {
      //     console.log(response, "receive");
      //     setuserData(response.data);
      // })
      // .catch((error) => {
      //     setuserError(error.message);
      // });
  }, []);


    const Edititem = (item) => {       
        console.log(item.id, "editing...")
        axios({
            method: "get",
            url: `https://5f5ca5325e3a4d0016249893.mockapi.io/users/${item.id}`,
            responseType: "json",
          })
          .then((response) => {
             console.log(response.data);
             console.log(response.data.Name);
             setName(response.data.Name);
             setCity(response.data.City);
             setAge(response.data.Age);
             setState(response.data.State);
             setCountry(response.data.Country);
             setEditdata(response.data);
          })
          .catch((error) => {
            // setError(error.message);
          });
    }

    const change = () => {
       if(Object.keys(Editdata).length !== 0)
       {
        //    Editdata["Name"] = Name,
        //    Editdata["City"] = City,
        //    Editdata["Age"] = Age,
        //    Editdata["State"] = State,
        //    Editdata["Country"] = Country
        Editdata.Name = Name
        Editdata.Age = Age
        Editdata.City = City
        Editdata.State = State
        Editdata.Country = Country
        console.log(Editdata);
       }
        //setEditdata({});
        console.log(Editdata, "submit value");
        axios({
            method: "PUT",
            url: `https://5f5ca5325e3a4d0016249893.mockapi.io/users/${Editdata.id}`,
            data: {
                id:Editdata.id,
                Name: Editdata.Name,
                Age: Editdata.Age,
                Country: Editdata.Country,
                State: Editdata.State,
                City: Editdata.City,
      },
    })
      .then((response) => {
        console.log(response, "success");
        setEditdata({});
      setAge("");
      setCity("");
      setCountry("");
      setName("");
      setState("");
      getmethod();
      })
      .catch((fail) => {
        console.log(fail, "fail");
      });
      
    }

    

    const Deleteitem = (item) =>{
        console.log(item.id, "deleting..");
        alert(item.id)
        axios({
            method: "DELETE",
            url: `https://5f5ca5325e3a4d0016249893.mockapi.io/users/${item.id}`,
          })
          .then((response) => {})
          .catch((error) => {
             // setError(error.message);
          });
    }


    return(
        <div  >
                <label style={{textAlign:"center"}} >User Editing Details</label>
                <label>Name</label>
                <input type="text" value={Name} onChange = {(e,index)=>{setName(e.target.value)}}  />
                <label>City</label>
                <input type="text" value={City} onChange = {(e,index)=>{setCity(e.target.value)}} />
                <label>Age</label>
                <input value={Age} onChange = {(e,index)=>{setAge(e.target.value)}} />
                <label>State</label>
                <input type="text" value={State} onChange = {(e,index)=>{setState(e.target.value)}} />
                <label>Country</label>
                <input type="text" value={Country} onChange = {(e,index)=>{setCountry(e.target.value)}} />
                <button onClick={change}>Changes Apply</button>
            <table >
            {userData.map((item,index) => (
          <tr key={item.id}  >
            {Object.values(item).map((val) => (
              <td>{val}</td>
            ))}
            <td style={{ textAlign: "center" }}>
              <button style={{ textAlign: "center" }} onClick ={() => {Edititem(item)}} >
                Edit
              </button>
            </td>
            <td style={{ textAlign: "center" }}>
              <button style={{ textAlign: "center" }}
                style={{ backgroundColor: "red", color: "#FFFFFF" }}
                onClick ={() => {Deleteitem(item)}}
              >
                Delete
              </button>
            </td>
          </tr>
        ))}
            </table>
        </div>
    )
}

export default Showintable;