import React from 'react'
import Props from './Props'
//mport React,{Component} from 'react'

// class Tutorial extends React.Component{

//     constructor(){
//         super();
//         this.state={    //state example
//             country:"India"
//         }
//     }

//      change=()=>{
//         this.setState({country:"England"})   //setState example
//     }

//     render(){
//         return <div>
//             <h1>SET STATE</h1>
//             <h1>This is {this.state.country}</h1>
//             <button onClick={this.change}>Apply changes</button>
//         </div>
//     }
// }


// export default Tutorial;
function Tutorial(){
    return(
        <div>
            <h1>Functional Props</h1>
            <Props language='props' />
        </div>
    )
}

export default Tutorial;