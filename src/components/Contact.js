
import React,{ useState, useEffect } from 'react'
import axios from "axios";

const Contact = () => {
    const [name, setname] = useState("");
    const [email, setemail] = useState("");
    const [data, setdata] = useState([]);
    const [selectindex, setselectedindex] = useState(null);
    
    const store = () => {
        if(selectindex !== null){
            update()
        }
        else{
            let datafortable=data;
        datafortable.push({
            id:name,
            name:name,
            email:email
        })
        setname("")
        setemail("")
        }
    }

    const update = () => {
        let updatedata = [];
        let olddata = data;
        olddata.map((item,index) => {
            if(index == selectindex){
                var obj = {
                id: name,
                name: name,
                email: email
                }
                updatedata.push(obj)
            }
            else{
                updatedata.push(item)
            }
        })
        setdata(updatedata)
        setselectedindex(null)
        setname("")
        setemail("")
    }

    const edit = (item,index) => {
        console.log(item.id,index);
        setname(item.name)
        setemail(item.email)
        setselectedindex(index)
    };
    const deleteitem = (index) => {
        console.log(index);
        alert(index)
        let deletedata = data;
        deletedata.splice(index,1);
        setdata([...deletedata]);
        
    };
    return(
        <div>
            <label>User Details</label>
            <label>Username</label>
            <input type="text" onChange = {(e,index)=>{setname(e.target.value)}} value={name} />
            <label>Email</label>
            <input type="email" onChange = {(e)=>{setemail(e.target.value)}} value={email} />
            <button onClick={store} >Submit</button>

            <label>Details</label>
            {data.length !== 0 &&
                <table>
                {data.map((item,index) => (
              <tr key={item.id}>
                {Object.values(item).map((val) => (
                  <td>{val}</td>
                ))}
                <td>
                  <button onClick={()=>{edit(item,index);}}>
                    Edit
                  </button>
                </td>
                <td>
                  <button onClick={() => {
                  deleteitem(index);
                }}>
                    Delete
                  </button>
                </td>
              </tr>
            ))}
                </table>
            }
            

        </div>
    )
}


export default Contact;